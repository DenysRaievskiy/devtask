from rest_framework import serializers
from shopping_lists.models import ShoppingList,ShoppingItem
from django.contrib.auth.models import User


class UserSerializers(serializers.ModelSerializer):
    """Serialization of user"""
    class Meta:
        model = User
        field =("id","email","password")


class ListSerializers(serializers.ModelSerializer):
    """Serialization of shopping list"""

    class Meta:
        model = ShoppingList
        fields = ("name","owner","date_created", "date_updated")

class ItemSerializers(serializers.ModelSerializer):
    """Serialization of shopping list"""

    class Meta:
        model = ShoppingItem
        fields = ("name","list_name","in_basket","date_created", "date_updated")