from django.db import models
#from django.contrib.auth.models import User
from accounts.models import User
# from djoser.urls.base import User

class ShoppingList(models.Model):
    """This is the model for a single shopping list"""
    name = models.CharField(max_length=30, null=False)
    owner = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='shopping_lists')
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-date_created',)

class ShoppingItem(models.Model):
    """This is the model for a single shopping item in a list"""
    name = models.CharField(max_length=30, null=False)
    in_basket = models.BooleanField(default=False)
    list_name = models.ForeignKey(
        ShoppingList, on_delete=models.CASCADE, related_name='shopping_items')
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-date_created',)




