from django.contrib.auth.views import (LoginView, LogoutView)
from django.views.generic import CreateView
from django.urls import reverse_lazy

# Create your views here.


class LoginView(LoginView):
    """Form view for Login"""
    template_name = "accounts/login-page.html"
    redirect_authenticated_user = True

class SignUpView(CreateView):
    """Creating a new non-super-user"""
    template_name = "accounts/sign-up-page.html"
    success_url = reverse_lazy("accounts:login")


class CustomLogoutView(LogoutView):
    """Cutomised logout view"""
    next_page = reverse_lazy("home_page")